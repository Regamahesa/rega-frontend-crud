import Swal from 'sweetalert2';
import React, { useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

export default function Register() {
    const history = useHistory()
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [fotoProfile, setFotoProfile] = useState("");
    const [namaKamu, setNamaKamu] = useState("");
    const [alamat, setAlamat] = useState("");
    const [noTelepon, setNoTelepon] = useState("")

    // const [userRegister, setUserRegister] = useState({
    //     email:'',
    //     password:'',
    //     role:'USER',
    // })

    // const handleOnChange = (e) => {
    //     setUserRegister((currUser)=> {
    //         return {...currUser, [e.target.id]: e.target.value}
    //     })
    // }

    const register = async (e) => {
        e.preventDefault();
        e.persist();
        const formData = new FormData();
        formData.append("file", fotoProfile)
        formData.append("email", email)
        formData.append("password", password)
        formData.append("role", "USER")
        formData.append("namaKamu", namaKamu)
        formData.append("alamat", alamat)
        formData.append("noTelepon", noTelepon)
        try {
            const response = await axios.post("http://localhost:3004/user/sign-up", formData, {

                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            }).then(() => {

                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil Registrasi',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(() => {
                    history.push('/login')
                }, 1250)
            })
        } catch (error) {
            console.log(error)
        }
    }

    // const  [username, setUsername] = useState("");
    // const [password, setPassword] = useState("");

    // const history = useHistory();

    // const addRegister = async (e) => {
    //     e.preventDefault();

    //     try {
    //         await axios.post("http://localhost:8000/users" ,{
    //             username: username,
    //             password: password
    //         });
    //         Swal.fire({
    //             icon: 'success',
    //             title: 'Your work has been saved',
    //             showConfirmButton: false,
    //             timer: 1500
    //           })
    //           setTimeout(() => {
    //           window.location.reload("/login");
    //           }, 1000);
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    return (
        <div className="kotak_login">
            <h1 className="mb-5">Register</h1>
            <Form onSubmit={register}>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Email Address</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="Email Address"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)} />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Password</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="Password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)} />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Foto Profile</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            type="file"
                            onChange={(e) => setFotoProfile(e.target.files[0])} />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Nama</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            type="text"
                            value={namaKamu}
                            onChange={(e) => setNamaKamu(e.target.value)} />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>alamat</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            type="text"
                            value={alamat}
                            onChange={(e) => setAlamat(e.target.value)} />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>No Hp :</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            value={noTelepon}
                            onChange={(e) => setNoTelepon(e.target.value)} />
                    </InputGroup>
                </div>
                <button variant="primary" type="submit" className="mx-1 button btn">
                    Register
                </button><br /><br />
                <center>
                    <a href="/login">Login</a>
                    <span >Jika belum memiliki akun</span>
                </center>
            </Form>
        </div>
    )
}
