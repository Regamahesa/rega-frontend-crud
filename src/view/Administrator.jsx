import axios from 'axios';
import "../style/Home.css";
import Swal from 'sweetalert2';
import NavigationBar from "../component/NavigationBar";
import Footer from "../component/Footer";
import React, { useEffect, useState } from 'react'
import { Container, Pagination, PaginationItem, PaginationLink, Table } from 'reactstrap';

export default function Home() {
    const [makanan, setMakanan] = useState([]);
    const [totalPages, setTotalPages] = useState([]);
    const [search, setSearch] = useState([]);

    // const getAll = async () => {
    //     await axios
    //         .get(`http://localhost:3004/product/all?nama=${makanan}&page=$page=${page}`)
    //         .then((res) => {
    //             setMakanan(res.data.data.content);

    //         }).catch((error) => {
    //             alert("Terjadi kesalahan " + error)
    //         })
    // }

    const getAll = async (page = 0) => {
        await axios
            .get(`http://localhost:3004/product/all?page=${page}&search=${search}`)
            .then((res) => {
                const pages = [];
                for (let index = 0; index < res.data.data.totalPages; index++) {
                    pages.push(index)       
                }
                setTotalPages(pages)
                setSearch(search)   
                setMakanan(res.data.data.content);
            }).catch((error) => {
                alert("Terjadi kesalahan " + error)
            })
    }
    const konversionRupiah = (angka) => {
        return new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0,
        }).format(angka)
    }
  

    useEffect(() => {
        getAll();
    }, [])

    const deleteFood = async (id) => {
        // Async-await bisa dikatakan sebagai cara mudah menggunakan JavaScript Promise yang agak sulit dipahami.

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete("http://localhost:3004/product/" + id)
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )

            }
        }).then(() => {
            window.location.reload();
        });


        // await Swal.fire({
        //     title: 'Are you sure?',
        //     text: "You won't be able to revert this!",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes, delete it!'
        // }).then((result) => {
        //     if (result.isConfirmed) {
        //         axios.delete("http://localhost:8000/daftarMakanan/" + id)
        //         Swal.fire(
        //             'Deleted!',
        //             'Your file has been deleted.',
        //             'success'
        //         )

        //     }
        // }) .then(() => {
        //         window.location.reload();
        //     });
    }

    return (
        <div>
            <NavigationBar/>
        <div className="container my-5 bs">
             <div class="d-flex" role="search">
        <input class="form-control me-2"
        type="search" 
        placeholder="Search" 
        aria-label="Search" 
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        />
        <button class="btn btn-outline-success" onClick={() => getAll()}>Search</button>
      </div>

    <br/>
            <Table responsive className="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama makanan</th>
                        <th>Deskripsi</th>
                        <th>Harga</th>
                        <th>Image</th>
                        {localStorage.getItem("role") === "ADMIN" ? <th>Action</th> : <></>}
                    </tr>
                </thead>
                <tbody>
                    {makanan.map((makanans, index) => (
                        <tr key={makanans.id}>
                            <td>{index + 1}</td>
                            <td>{makanans.nama}</td>
                            <td>{makanans.deskripsi}</td>
                            <td>{konversionRupiah(makanans.harga)}</td>
                            <td>
                                <span>
                                    <img src={makanans.img} alt="" width={100} height={100}/>
                                </span>
                            </td>
                            {localStorage.getItem("role") === "ADMIN" ? <td>
                                <a href={"/edit/" + makanans.id}>
                                    <button
                                        variant="warning"
                                        className="mx-1"
                                        style={{ backgroundColor: "green" }} >
                                        <i class="fas fa-edit"></i> 
                                    </button>
                                </a>
                                | |
                                <button
                                    variant="danger"
                                    className="mx-1"
                                    style={{ backgroundColor: "red" }}
                                    onClick={() => deleteFood(makanans.id)}
                                >
                                <i class="fas fa-trash-alt"></i>
                                </button>
                            </td> : <></>}
                        </tr>
                    ))}
                </tbody>
            </Table>


            <Container className="pag">
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                        <PaginationLink
                            first
                            href="#"
                        />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink previous />
                    </PaginationItem>
                    {
                        totalPages.map((data, index) => (
                            <PaginationItem key={index}>
                            <PaginationLink onClick={() => getAll(data)}>
                                {data+1}
                            </PaginationLink>
                        </PaginationItem>
                        ))
                    }
                   
                    <PaginationItem>
                        <PaginationLink next />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink
                            href="#"
                            last
                        />
                    </PaginationItem>
                </Pagination>
            </Container>

        </div>
        <Footer />
        </div>
    )
}
