import axios from 'axios';
import Swal from 'sweetalert2';
import React, { useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap'
import { useHistory, useParams } from 'react-router-dom'
    
    export default function Edit() {
        const param = useParams();
        const [deskripsi, setDeskripsi] = useState("");
        const [nama, setNama] = useState("");
        const [img, setImg] = useState("");
        const [harga, setHarga] = useState("");

        const history = useHistory();

        useEffect(() => {
            axios.get("http://localhost:3004/product/" + param.id)
            .then((response) =>{
                const newFood = response.data.data;
                setNama(newFood.nama);
                setDeskripsi(newFood.deskripsi);
                setImg(newFood.img);
                setHarga(newFood.harga);
            })
            .catch((error) => {
                alert("terjadi kesalahan sir " + error)
            })
        }, []);


        const submitActionHandler = async (e) => {
          e.preventDefault();
          const formData = new FormData();
          formData.append("file", img)
          formData.append("nama", nama)
          formData.append("deskripsi", deskripsi)
          formData.append("harga", harga)
          Swal.fire({
                  title: 'apakah yakin di edit datanya?',
                  showCancelButton: true,
                  confirmButtonText: 'Edit',
        }).then((result) => {
          if (result.isConfirmed) {
           axios.put("http://localhost:3004/product/" + param.id , formData, 
         {
          headers: {
            Authorization:`Bearer ${localStorage.getItem("token")}`,
            " Content-Type": "multipart/form-data",
          }
            }
            ).then(() => {
              history.push("/administrator");
              Swal.fire('Berhasil Mengedit!', '', 'success')
            }).catch((error) => {
              console.log(error);
            })
          }
        })
        }

      return (
        <div className="edit mx-5">
        <div className="container my-5">
          <form onSubmit={submitActionHandler}>
            <div className="name mb-3">
              <Form.Label>
                <strong>Nama makanan</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Nama makanan"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                />
              </InputGroup>
            </div>
  
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Deskripsi"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)} />
              </InputGroup>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Img</strong>
              </Form.Label>
              <div className="d-flex gap-3">
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                  required
                  type="file"
                    onChange={(e) => setImg(e.target.files[0])} />
                </InputGroup>
              </div>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Harga</strong>
              </Form.Label>
              <div className="d-flex gap-3">
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="Harga"
                    value={harga}
                    onChange={(e) => setHarga(e.target.value)} />
                </InputGroup>
              </div>
            </div>
            <div className="d-flex justify-conten-end align-items-center mt-2 text-blue-600">
              <button className="buton btn" >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
      )
    }
    