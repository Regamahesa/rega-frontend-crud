import React from 'react'
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Swal from 'sweetalert2';
import { Container, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import "../style/Menu.css";
import Navigation from "../component/NavigationBar";
import { useEffect, useState } from 'react'
import Footer from '../component/Footer';

export default function Menu() {
    const [makanan, setMakanan] = useState([]);
    const [totalPages, setTotalPages] = useState([]);
    const [search, setSearch] = useState([]);

    const getAll = async (page = 0) => {
        await axios
            .get(`http://localhost:3004/product/all?page=${page}&search=${search}`)
            .then((res) => {
                setMakanan(res.data.data.content.map((item) => ({
                    ...item,
                    qty: 1,
                })));
                const data = [];
                for (let index = 0; index < res.data.data.totalPages; index++) {
                    data.push(index)
                }
                setTotalPages(data)
                setSearch(search)   
            }).catch((error) => {
                alert("Terjadi kesalahan " + error)
            })
    }

    const plusQuantityProduct = (idx) => {
        const listProduct = [...makanan];
        if (listProduct[idx].qty >= 45 ) return;
        listProduct[idx].qty++;
       setMakanan(listProduct)
    }
    const minusQuantityProduct = (idx) => {
        const listProduct = [...makanan];
        listProduct[idx].qty--;
        if (listProduct[idx].qty < 1) {
            listProduct[idx].qty = 1;
            return;
        }
       setMakanan(listProduct)
    }

    const konversionRupiah = (angka) => {
        return new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0,
        }).format(angka)
    }

    const checkOut = async (food) => {
        console.log(food);
        await axios.post("http://localhost:3004/cart", {
            "productId": food.id,
            "qty": food.qty,
            "userId": localStorage.getItem("userId"),
        });
        Swal.fire({
            icon: 'success',
            title: 'Berhasil memasukkan ke keranjang',
            showConfirmButton: false,
            timer: 1500
          })
    }

    useEffect(() => {
        getAll();
    }, [])

    return (
        <div>
            <Navigation/>
            <div className="d-flex container my-5 bs" role="search">
        <input className="form-control me-2"
        type="search" 
        placeholder="Search" 
        aria-label="Search" 
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        />
        <button className="btn btn-outline-success" onClick={() => getAll()}>Search</button>
      </div>
            <div>
                <div className="text-center"><h1>Silahkan pilih menu yang tersedia</h1></div>
            </div>
            <div className="menu  grid grid-cols-4 gap-3 container my-5 bs">
                {makanan.map((food, idx) => {
                    return (
                        <Card key={food.id} style={{ width: '18rem' }}>
                            <Card.Img variant="top" src={food.img} width={150} height={150} />
                            <Card.Body>
                                <Card.Title>{food.nama}</Card.Title>
                                <br />
                                <Card.Text>{food.deskripsi}</Card.Text>
                                <br />
                                <Card.Text>{konversionRupiah(food.harga)}</Card.Text>
                                <br />
                                <div>
                                    <div className="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" className="btn " onClick={() =>  minusQuantityProduct(idx)}>-</button>
                                        <button className="btn">{food.qty}</button>
                                        <button type="button" className="btn " onClick={() => plusQuantityProduct(idx)}>+</button>
                                    </div>
                                </div>
                                <br />
                                {localStorage.getItem("role") !== null ? <Button variant="primary" onClick={() => checkOut(food)}>Buy</Button> : <></>}
                            </Card.Body>
                        </Card>
                    )
                })}
            </div>
            <br />
            <Container className="pag">
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                        <PaginationLink
                            first
                            href="#"
                        />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink previous />
                    </PaginationItem>
                    {
                        totalPages.map((data, index) => (
                            <PaginationItem key={index}>
                                <PaginationLink onClick={() => getAll(data)}>
                                    {data + 1}
                                </PaginationLink>
                            </PaginationItem>
                        ))
                    }

                    <PaginationItem>
                        <PaginationLink next />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink
                            href="#"
                            last
                        />
                    </PaginationItem>
                </Pagination>
            </Container>
            <Footer />
        </div>
    )
}
