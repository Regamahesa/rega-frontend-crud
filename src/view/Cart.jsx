import React, { useState, useEffect } from 'react'
import { Table } from "reactstrap";
import axios from 'axios';
import { Container, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Swal from 'sweetalert2';
import "../style/Cart.css"
export default function Cart() {
    const [makanan, setMakanan] = useState([]);
    const [totalHarga, setTotalHarga] = useState(0);
    const [totalPages, setTotalPages] = useState([])

    const getAll = (page = 0) => {
        axios.get(`http://localhost:3004/cart?page=${page}&users_id=${localStorage.getItem("userId")}`)
        .then((res) => {
            setMakanan(res.data.data.content);
            const data = [];
            for (let index = 0; index < res.data.data.totalPages; index++) {
                data.push(index)       
            }
            setTotalPages(data)
            let total = 0;
            res.data.data.content.forEach((item) => {
                total += item.totalHarga;
            });
            setTotalHarga(total)
        }).catch((error) => {
            alert("Terjadi kesalahan " + error)
        })
    };


    const konversionRupiah = (angka) => {
        return new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0,
        }).format(angka)
    }
  
    const checkOutProduct = async () => {
        Swal.fire({
            title: 'Apakah anda Ingin checkout?',
            text: "Bisa Dilihat-lihat lagi!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, checkout!'
        }).then((result) => {
            if (result.isConfirmed) {
                 axios.delete(`http://localhost:3004/cart?userId=${localStorage.getItem("userId")}`)
                Swal.fire(
                    'Checkouted!',
                    'Maknananmu sudah di checkout.',
                    'success'
                )

            }
        })
    }

    useEffect(() => {
        getAll();
    }, [])

    const deleteFood = async (id) => {
        // Async-await bisa dikatakan sebagai cara mudah menggunakan JavaScript Promise yang agak sulit dipahami.

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete("http://localhost:3004/cart/" + id)
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )

            }
        }).then(() => {
            window.location.reload();
        });
    }
    
    return (
        <div>
            <Table className="table">
            <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama makanan</th>
                        <th>Deskripsi</th>
                        <th>Harga</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
            </thead>
            <tbody>
                {makanan.map((data, idx) => {
                    return (
                        <tr key={idx}>
                            <td>{idx +1}</td>
                            <td>{data.productsId.nama}</td>
                            <td>{data.productsId.deskripsi}</td>
                            <td>{konversionRupiah(data.productsId.harga)}</td>
                            <td>
                                <span>
                                    <img src={data.productsId.img} alt="" width={100} height={100}/>
                                </span>
                            </td>
                            <td>
                            <button
                                    variant="danger"
                                    className="mx-1"
                                    style={{ backgroundColor: "red" }}
                                    onClick={() => deleteFood(data.id)}
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
            </Table>
            <strong>Total harga : {konversionRupiah(totalHarga)}</strong>
            <br />
            <br />
            <button className='btn btn-warning'  onClick={checkOutProduct}>Checkout</button>
                <br />
                <br />
                <a className='btn btn-warning' style={{textAlign: "center"}} aria-current="page" href="/menu">Kembali</a>
                <br />
                <br />
                <Container className="pag">
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                        <PaginationLink
                            first
                            href="#"
                        />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink previous />
                    </PaginationItem>
                    {
                        totalPages.map((data, index) => (
                            <PaginationItem key={index}>
                            <PaginationLink onClick={() => getAll(data)}>
                                {data+1}
                            </PaginationLink>
                        </PaginationItem>
                        ))
                    }
                   
                    <PaginationItem>
                        <PaginationLink next />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink
                            href="#"
                            last
                        />
                    </PaginationItem>
                </Pagination>
            </Container>
        </div>
    )
}
