import './App.css';
import Dashboard from './pages/Home';
import Home from "./view/Administrator";
import Menu from "./view/Menu";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Edit from "./view/Edit";
import Profile from "./component/Profile";
import Cart from "./view/Cart";
import { BrowserRouter, Route, Switch } from 'react-router-dom'


function App() {
  return (
    <div className="App">
      
    
    <BrowserRouter>
    <main>
      <Switch>
        <Route path="/administrator" component={Home} exact/>
        <Route path="/" component={Dashboard} exact/>
        <Route path="/menu" component={Menu} exact/>
        <Route path="/login" component={Login} exact/>
        <Route path="/register" component={Register} exact/>
        <Route path="/edit/:id" component={Edit} exact/>
        <Route path="/profile" component={Profile} exact/>
        <Route path="/cart" component={Cart}/>
      </Switch>
    </main>
    </BrowserRouter>
    </div>
  );
}

export default App;
