import axios from 'axios';
import React, { useEffect, useState } from 'react'
import "../style/Profile.css"
export default function Profile() {
    const [user, setUser] = useState([]);

    const getUser = async () => {
        await axios.get("http://localhost:3004/user/" + localStorage.getItem("userId"))
        .then((res) => {
            setUser(res.data.data)
        }).catch((err) => {
            console.log(err)
        });
    }
    console.log(user);
    useEffect(() => {
        getUser();
    }, [])

    return (
        <div>
            <div>
            <a className='btn btn-warning' style={{textAlign: "center"}} aria-current="page" href="/">Kembali</a>
            </div>
            <div className="row py-5 px-4">
                <div className="col-xl-4 col-md-6 col-sm-10 mx-auto">
               
                        <div className="bg-white shadow rounded overflow-hidden">
                        <div className="px-4 pt-0 pb-4 bg-dark">
                            <div className="media align-items-end profile-header">
                                <div className="profile mr-3"><img src={user.fotoProfile} alt="..." width="130" className="rounded mb-2 img-thumbnail" /><a href="#" className="btn btn-dark btn-sm btn-block">Edit profile</a></div>
                                <div className="media-body mb-5 text-white">
                                    <h4 className="mt-0 mb-0">{user.namaKamu}</h4>
                                    <p className="small mb-4"> <i className="fa fa-map-marker mr-2"></i>{user.alamat}</p>
                                    <p className="small mb-4">NoHp: {user.noTelepon}</p>
                                </div>
                            </div>
                        </div>

                        <div className="bg-light p-4 d-flex justify-content-end text-center">
                            <ul className="list-inline mb-0">
                                <li className="list-inline-item">
                                    <h5 className="font-weight-bold mb-0 d-block">241</h5><small className="text-muted"> <i className="fa fa-picture-o mr-1"></i>Photos</small>
                                </li>
                                <li className="list-inline-item">
                                    <h5 className="font-weight-bold mb-0 d-block">84K</h5><small className="text-muted"> <i className="fa fa-user-circle-o mr-1"></i>Followers</small>
                                </li>
                            </ul>
                        </div>

                        <div className="py-4 px-4">
                            <div className="d-flex align-items-center justify-content-between mb-3">
                                <h5 className="mb-0">Menu Terlaris</h5><a href="#" className="btn btn-link text-muted">Show all</a>
                            </div>
                            <div className="row">
                                <div className="col-lg-6 mb-2 pr-lg-1"><img src="https://img.kurio.network/SWLLBHjU19SqhKTasbyLc7sbw6U=/1200x900/filters:quality(80)/https://kurio-img.kurioapps.com/21/09/06/2c552606-f62f-475d-81db-e9a57e963a3f.jpe" alt="" className="img-fluid rounded shadow-sm" /></div>
                                <div className="col-lg-6 mb-2 pl-lg-1"><img src="https://firebasestorage.googleapis.com/v0/b/upload-image-examle.appspot.com/o/5fbbca027b1c8?alt=media" alt="" className="img-fluid rounded shadow-sm" /></div>
                                <div className="col-lg-6 pr-lg-1 mb-2"><img src="https://firebasestorage.googleapis.com/v0/b/upload-image-examle.appspot.com/o/download?alt=media" alt="" className="img-fluid rounded shadow-sm" /></div>
                                <div className="col-lg-6 pl-lg-1"><img src="https://firebasestorage.googleapis.com/v0/b/upload-image-examle.appspot.com/o/bubur-ayam-kuning-e1666848225186?alt=media" alt="" className="img-fluid rounded shadow-sm" /></div>
                            </div>  
                            <div className="py-4">
                                <h5 className="mb-3">Recent posts</h5>
                                <div className="p-4 bg-light rounded shadow-sm">
                                    <p className="font-italic mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                    <ul className="list-inline small text-muted mt-3 mb-0">
                                        <li className="list-inline-item"><i className="fa fa-comment-o mr-2"></i>12 Comments</li>
                                        <li className="list-inline-item"><i className="fa fa-heart-o mr-2"></i>200 Likes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
    )
}
